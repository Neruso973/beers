/*
  Warnings:

  - A unique constraint covering the columns `[veifyMailToken]` on the table `User` will be added. If there are existing duplicate values, this will fail.

*/
-- AlterTable
ALTER TABLE "User" ADD COLUMN     "isActive" BOOLEAN NOT NULL DEFAULT false,
ADD COLUMN     "veifyMailToken" VARCHAR(255);

-- CreateIndex
CREATE UNIQUE INDEX "User_veifyMailToken_key" ON "User"("veifyMailToken");
