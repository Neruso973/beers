-- CreateEnum
CREATE TYPE "SourceEnum" AS ENUM ('FRONT', 'BACK');

-- CreateEnum
CREATE TYPE "CategoryEnum" AS ENUM ('DEBUG', 'INFO', 'DEFAULT', 'WARNING', 'ERROR');

-- CreateEnum
CREATE TYPE "RoleEnum" AS ENUM ('ADMIN', 'USER', 'COMTE_DEV', 'DEV', 'DATA');

-- CreateTable
CREATE TABLE "Log" (
    "id" SERIAL NOT NULL,
    "source" "SourceEnum" NOT NULL,
    "category" "CategoryEnum" NOT NULL,
    "component" VARCHAR(255) NOT NULL,
    "message" VARCHAR(255) NOT NULL,
    "error" VARCHAR(255) NOT NULL,
    "date" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,

    CONSTRAINT "Log_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "User" (
    "id" SERIAL NOT NULL,
    "email" VARCHAR(255) NOT NULL,
    "firstname" VARCHAR(255) NOT NULL,
    "lastname" VARCHAR(255) NOT NULL,
    "username" VARCHAR(255) NOT NULL,
    "password" VARCHAR(255) NOT NULL,
    "resetPasswordToken" VARCHAR(255),
    "birth" TIMESTAMP(3),
    "created_at" TIMESTAMP(3) NOT NULL DEFAULT CURRENT_TIMESTAMP,
    "role" "RoleEnum"[],

    CONSTRAINT "User_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Address" (
    "id" SERIAL NOT NULL,
    "address" VARCHAR(255) NOT NULL,
    "addressComplement" VARCHAR(255) NOT NULL,
    "city" VARCHAR(255) NOT NULL,
    "zipCode" VARCHAR(255) NOT NULL,
    "country" VARCHAR(255) NOT NULL,
    "userId" INTEGER NOT NULL,

    CONSTRAINT "Address_pkey" PRIMARY KEY ("id")
);

-- CreateIndex
CREATE UNIQUE INDEX "User_email_key" ON "User"("email");

-- CreateIndex
CREATE UNIQUE INDEX "User_username_key" ON "User"("username");

-- CreateIndex
CREATE UNIQUE INDEX "Address_userId_key" ON "Address"("userId");

-- AddForeignKey
ALTER TABLE "Address" ADD CONSTRAINT "Address_userId_fkey" FOREIGN KEY ("userId") REFERENCES "User"("id") ON DELETE RESTRICT ON UPDATE CASCADE;
