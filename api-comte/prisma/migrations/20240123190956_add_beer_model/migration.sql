-- CreateTable
CREATE TABLE "Beer" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,

    CONSTRAINT "Beer_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Malt" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,
    "ebc" INTEGER NOT NULL,

    CONSTRAINT "Malt_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "Hop" (
    "id" SERIAL NOT NULL,
    "name" VARCHAR(255) NOT NULL,

    CONSTRAINT "Hop_pkey" PRIMARY KEY ("id")
);

-- CreateTable
CREATE TABLE "_BeerToMalt" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateTable
CREATE TABLE "_BeerToHop" (
    "A" INTEGER NOT NULL,
    "B" INTEGER NOT NULL
);

-- CreateIndex
CREATE UNIQUE INDEX "_BeerToMalt_AB_unique" ON "_BeerToMalt"("A", "B");

-- CreateIndex
CREATE INDEX "_BeerToMalt_B_index" ON "_BeerToMalt"("B");

-- CreateIndex
CREATE UNIQUE INDEX "_BeerToHop_AB_unique" ON "_BeerToHop"("A", "B");

-- CreateIndex
CREATE INDEX "_BeerToHop_B_index" ON "_BeerToHop"("B");

-- AddForeignKey
ALTER TABLE "_BeerToMalt" ADD CONSTRAINT "_BeerToMalt_A_fkey" FOREIGN KEY ("A") REFERENCES "Beer"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_BeerToMalt" ADD CONSTRAINT "_BeerToMalt_B_fkey" FOREIGN KEY ("B") REFERENCES "Malt"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_BeerToHop" ADD CONSTRAINT "_BeerToHop_A_fkey" FOREIGN KEY ("A") REFERENCES "Beer"("id") ON DELETE CASCADE ON UPDATE CASCADE;

-- AddForeignKey
ALTER TABLE "_BeerToHop" ADD CONSTRAINT "_BeerToHop_B_fkey" FOREIGN KEY ("B") REFERENCES "Hop"("id") ON DELETE CASCADE ON UPDATE CASCADE;
