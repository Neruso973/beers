import { Injectable } from '@nestjs/common';
import { CreateHopDto } from './dto/create-hop.dto';
import { UpdateHopDto } from './dto/update-hop.dto';

@Injectable()
export class HopService {
  create(createHopDto: CreateHopDto) {
    return 'This action adds a new hop';
  }

  findAll() {
    return `This action returns all hop`;
  }

  findOne(id: number) {
    return `This action returns a #${id} hop`;
  }

  update(id: number, updateHopDto: UpdateHopDto) {
    return `This action updates a #${id} hop`;
  }

  remove(id: number) {
    return `This action removes a #${id} hop`;
  }
}
