import { Module } from '@nestjs/common';
import { HopService } from './hop.service';
import { HopController } from './hop.controller';

@Module({
  controllers: [HopController],
  providers: [HopService],
})
export class HopModule {}
