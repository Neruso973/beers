import { Module } from '@nestjs/common';
import { LogController } from './log.controller';
import { SharedModule } from 'src/shared/shared.module';

@Module({
  controllers: [LogController],
  imports: [SharedModule],
})
export class LogModule {}
