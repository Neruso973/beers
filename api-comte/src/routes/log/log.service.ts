import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CategoryEnum, CreateLogDto, SourceEnum } from './data/dto/create-log.dto';
import { LogDto } from './data/dto/log.dto';
import { PrismaService } from 'src/shared/services/prisma.service';
import { CreateLog } from './data/model/create-log';
import { dtoToCreateLogModel, dtoToLogModel, modelToLogDto } from './data/mapper/log.mapper';
import { Log } from './data/model/log';
import { CreateLogBuilder } from './data/dto/create-log.builder';
import { apiErrors } from 'src/shared';

@Injectable()
export class LogService {
  constructor(private _prisma: PrismaService) {}
  
  async create(createLogDto: CreateLogDto): Promise<LogDto> {
    const createLog: CreateLog = dtoToCreateLogModel(createLogDto);
    const createdLog = await this._prisma.log.create({
      data: createLog,
    });
    if (!createdLog) {
      throw new BadRequestException(apiErrors.logs.notCreated);
    }
    return modelToLogDto(createdLog);
  }

  async findAll(): Promise<LogDto[]> {
    const logs = await this._prisma.log.findMany();
    if (!logs.length) {
      throw new NotFoundException(apiErrors.logs.notFound);
    }

    const result: LogDto[] = logs.map((log) => modelToLogDto(log));

    return result;
  }

  async findOne(id: number): Promise<LogDto> {
    const log = await this._prisma.log.findUnique({ where: { id } });
    if (!log) {
      throw new NotFoundException(apiErrors.logs.notFound);
    }
    return modelToLogDto(log);
  }

  async update(id: number, updateLogDto: LogDto): Promise<LogDto> {
    const createLogModel:Log = dtoToLogModel(updateLogDto);
    
   //check if log to update is the same that the one in the body
   if (id !== createLogModel.id) {
    throw new BadRequestException(apiErrors.logs.notUpdated);
  }

  //check if log to update exists
  const logToUpdate = await this.findOne(id);
  if (!logToUpdate) {
    throw new NotFoundException(apiErrors.logs.notFoundToUpdate);
  }

  //update log
  const createlog = await this._prisma.log.update({
    where: { id },
    data: createLogModel,
  });
  if (!createlog) {
    throw new BadRequestException(apiErrors.logs.notUpdated);
  }

  return modelToLogDto(createlog);

  }

  async remove(id: number): Promise<LogDto> {
     //check if log to update exists
     const logToDelete = await this.findOne(id);
     if (!logToDelete) {
       throw new NotFoundException(apiErrors.logs.notFoundToDelete);
     }
 
     const deletedLog = await this._prisma.log.delete({
       where: { id },
     });
     return modelToLogDto(deletedLog);
  }

  async log(err: any, constructor: string, logType: CategoryEnum, message?: string) {
    console.log(err);
    const newLog: CreateLogDto = new CreateLogBuilder()
    .withSource(SourceEnum.BACK)
    .withCategory(logType)
    .withComponent(constructor)
    .withError(err?.response?.error ?? "unknown error")
    .withMessage(err?.response?.message ?? message ?? "unknown message")
    .withDate(new Date())
    .build();
    
    await this.create(newLog);
  }
}
