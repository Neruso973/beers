import { Prisma } from '@prisma/client';
import { CategoryEnum, SourceEnum } from '../dto/create-log.dto';

interface LogInterface extends Prisma.LogCreateInput {}

export class CreateLog implements LogInterface {
  source: SourceEnum;
  category: CategoryEnum;
  component: string;
  message: string;
  error: string;
  date: Date;
}
