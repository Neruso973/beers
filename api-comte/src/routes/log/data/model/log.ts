import { Prisma } from '@prisma/client';
import { CreateLog } from './create-log';

interface LogInterface extends Prisma.LogUpdateInput {}

export class Log extends CreateLog implements LogInterface {
  id: number;
}
