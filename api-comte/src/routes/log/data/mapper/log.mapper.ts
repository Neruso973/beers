import { MapperService } from 'src/shared/services/mapper.service';
import { CreateLogDto } from '../dto/create-log.dto';
import { LogDto } from '../dto/log.dto';
import { CreateLog } from '../model/create-log';
import { Log } from '../model/log';

// Fonctions spécifiques de conversion
export function dtoToCreateLogModel(
  createLogDto: CreateLogDto
): CreateLog {
  return MapperService.convertObject(createLogDto, CreateLog);
}

export function dtoToLogModel(logDto: LogDto): Log {
  return MapperService.convertObject(logDto, Log);
}

export function modelToLogDto(prismaModel: any): LogDto {
  return MapperService.convertObject(prismaModel, LogDto);
}