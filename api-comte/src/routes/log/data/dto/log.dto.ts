import { CreateLogDto } from './create-log.dto';

export class LogDto extends CreateLogDto {
    id: number;
}
