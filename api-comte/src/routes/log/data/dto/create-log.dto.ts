
export class CreateLogDto {
  source: SourceEnum
  category: CategoryEnum
  component: string 
  message: string 
  error: string 
  date: Date
}

export enum SourceEnum {
  FRONT = 'FRONT',
  BACK = 'BACK'
}

export enum CategoryEnum {
  DEBUG = 'DEBUG',
  INFO = 'INFO',
  DEFAULT = 'DEFAULT',
  WARNING = 'WARNING',
  ERROR = 'ERROR'
}

