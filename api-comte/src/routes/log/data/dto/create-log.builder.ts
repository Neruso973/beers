import { CreateLogDto, SourceEnum, CategoryEnum } from "./create-log.dto";

export class CreateLogBuilder {
  private createLog: CreateLogDto;

constructor() {
    this.createLog = new CreateLogDto();
}

withSource(source: SourceEnum): CreateLogBuilder {
    this.createLog.source = source;
    return this;
}

withCategory(category: CategoryEnum): CreateLogBuilder {
    this.createLog.category = category;
    return this;
}

withComponent(component: string): CreateLogBuilder {
    this.createLog.component = component;
    return this;
}

  withMessage(message: string): CreateLogBuilder {
    this.createLog.message = message;
    return this;
  }

  withError(error: string): CreateLogBuilder {
    this.createLog.error = error;
    return this;
  }

  withDate(date: Date): CreateLogBuilder {
    this.createLog.date = date;
    return this;
  }

  build(): CreateLogDto {
    return this.createLog;
  }
}
