import { Controller, Get, Post, Body, Patch, Param, Delete, UsePipes } from '@nestjs/common';
import { LogService } from './log.service';
import { CreateLogDto } from './data/dto/create-log.dto';
import { LogDto } from './data/dto/log.dto';
import { ApiTags } from '@nestjs/swagger';
import { ParamToNumberPipe } from 'src/shared/pipes/param-to-number.pipe';

@ApiTags('log')
@Controller('log')
export class LogController {
  constructor(private readonly logService: LogService) {}

  @Post()
  async create(@Body() createLogDto: CreateLogDto): Promise<LogDto> {
    try {
      const newLog = await this.logService.create(createLogDto);
      return newLog;
    } catch (error) {
      return error;
    }
  }

  @Get()
  async findAll(): Promise<LogDto[]> {
    try {
      const logs = await this.logService.findAll();
      return logs;
    } catch (error) {
      return error;
    }
  }

  @Get(':id')
  @UsePipes(ParamToNumberPipe)
  async findOne(@Param('id') id: number): Promise<LogDto> {
    try {
      const log = await this.logService.findOne(id);
      return log;
    } catch (error) {
      return error;
    }
  }

  @Patch(':id')
  @UsePipes(ParamToNumberPipe)
  async update(@Param('id') id: number, @Body() updateLogDto: LogDto) {
    try {
      return this.logService.update(id, updateLogDto);
    } catch (error) {
      return error;
    }
  }

  @Delete(':id')
  @UsePipes(ParamToNumberPipe)
  async remove(@Param('id') id: number) {
    try {
      return this.logService.remove(id);
    } catch (error) {
      return error;
    }
  }
}
