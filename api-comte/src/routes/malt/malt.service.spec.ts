import { Test, TestingModule } from '@nestjs/testing';
import { MaltService } from './malt.service';

describe('MaltService', () => {
  let service: MaltService;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      providers: [MaltService],
    }).compile();

    service = module.get<MaltService>(MaltService);
  });

  it('should be defined', () => {
    expect(service).toBeDefined();
  });
});
