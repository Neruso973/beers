import { Module } from '@nestjs/common';
import { MaltService } from './malt.service';
import { MaltController } from './malt.controller';

@Module({
  controllers: [MaltController],
  providers: [MaltService],
})
export class MaltModule {}
