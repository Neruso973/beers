import { Injectable } from '@nestjs/common';
import { CreateMaltDto } from './dto/create-malt.dto';
import { UpdateMaltDto } from './dto/update-malt.dto';

@Injectable()
export class MaltService {
  create(createMaltDto: CreateMaltDto) {
    return 'This action adds a new malt';
  }

  findAll() {
    return `This action returns all malt`;
  }

  findOne(id: number) {
    return `This action returns a #${id} malt`;
  }

  update(id: number, updateMaltDto: UpdateMaltDto) {
    return `This action updates a #${id} malt`;
  }

  remove(id: number) {
    return `This action removes a #${id} malt`;
  }
}
