import { PartialType } from '@nestjs/swagger';
import { CreateMaltDto } from './create-malt.dto';

export class UpdateMaltDto extends PartialType(CreateMaltDto) {}
