import { Test, TestingModule } from '@nestjs/testing';
import { MaltController } from './malt.controller';
import { MaltService } from './malt.service';

describe('MaltController', () => {
  let controller: MaltController;

  beforeEach(async () => {
    const module: TestingModule = await Test.createTestingModule({
      controllers: [MaltController],
      providers: [MaltService],
    }).compile();

    controller = module.get<MaltController>(MaltController);
  });

  it('should be defined', () => {
    expect(controller).toBeDefined();
  });
});
