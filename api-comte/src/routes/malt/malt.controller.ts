import { Controller, Get, Post, Body, Patch, Param, Delete } from '@nestjs/common';
import { MaltService } from './malt.service';
import { CreateMaltDto } from './dto/create-malt.dto';
import { UpdateMaltDto } from './dto/update-malt.dto';

@Controller('malt')
export class MaltController {
  constructor(private readonly maltService: MaltService) {}

  @Post()
  create(@Body() createMaltDto: CreateMaltDto) {
    return this.maltService.create(createMaltDto);
  }

  @Get()
  findAll() {
    return this.maltService.findAll();
  }

  @Get(':id')
  findOne(@Param('id') id: string) {
    return this.maltService.findOne(+id);
  }

  @Patch(':id')
  update(@Param('id') id: string, @Body() updateMaltDto: UpdateMaltDto) {
    return this.maltService.update(+id, updateMaltDto);
  }

  @Delete(':id')
  remove(@Param('id') id: string) {
    return this.maltService.remove(+id);
  }
}
