import {
  Controller,
  Post,
  Body,
  Req,
  Get,
  ImATeapotException,
  UsePipes,
  Res,
} from '@nestjs/common';
import { ApiQuery, ApiTags } from '@nestjs/swagger';
import { ConfigService } from '@nestjs/config';
import { Request, Response } from 'express';
import { JoiValidationPipe, ReturnObject, apiErrors } from 'src/shared';
import { AuthService } from './auth.service';
import { LogService } from '../log/log.service';
import { CategoryEnum } from '../log/data/dto/create-log.dto';
import { RoleEnum } from '../users/data';
import {
  signupSchema,
  SignupUserDto,
  AuthDto,
  signinSchema,
  LoginUserDto,
  forgotPasswordSchema,
  ForgotPasswordDto,
  resetPasswordSchema,
  ResetPasswordDto,
  ValidateEmailDto,
} from './data';

@ApiTags('api/auth')
@Controller('api/auth')
export class AuthController {
  constructor(
    private readonly _authService: AuthService,
    private _config: ConfigService,
    private readonly _log: LogService,
  ) {}

  //sign up
  @Post('signup')
  @UsePipes(new JoiValidationPipe(signupSchema))
  async signup(
    @Body() signupUser: SignupUserDto,
    @Req() req: Request,
  ): Promise<ReturnObject> {
    try {
      let role = [RoleEnum.USER];

      const currentUser = (req as any)?.user;
      // admin can create admin
      if (currentUser && currentUser.role.includes(RoleEnum.ADMIN)) {
        console.log(req.query);
      }

      const result = await this._authService.signup(signupUser, role);
      if (!result) {
        return null;
      }
      return result;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  //sign in
  @Post('signin')
  @UsePipes(new JoiValidationPipe(signinSchema))
  async signin(@Body() signinUser: LoginUserDto): Promise<AuthDto> {
    try {
      const result = await this._authService.signin(signinUser);
      return result;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }
  //forgot password
  @Post('forgot-password')
  @UsePipes(new JoiValidationPipe(forgotPasswordSchema))
  async forgotPassword(
    @Body() data: ForgotPasswordDto,
    @Res() res: Response,
  ): Promise<any> {
    try {
      const result = await this._authService.forgotPassword(data.email);
      res.status(200).json({ code: result.code, message: result.message });
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  //reset password
  @Post('reset-password')
  @UsePipes(new JoiValidationPipe(resetPasswordSchema))
  async resetPassword(@Body() data: ResetPasswordDto): Promise<AuthDto> {
    try {
      const result = await this._authService.resetPassword(data);
      return result;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  /**
   * @description verify email
   * @param params 
   * @returns {AuthDto}
   */
  @Post('verify-email')
  async verifyEmail(@Body() data: ValidateEmailDto): Promise<AuthDto> {
    try {
      const result = await this._authService.verifyEmail(data);
      return result;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  /**
   * @description resend verify email
   * @param params 
   * @returns {ReturnObject}
   */
  @Post('resend-verify-email')
  async resendVerifyEmail(@Body() data: ForgotPasswordDto): Promise<ReturnObject> {
    try {
      const result = await this._authService.sendVerifyEmail(data.email);
      return result;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  //generate access token
  @Get('generate')
  @ApiQuery({
    name: 'where[role][has]',
    required: false,
    type: String,
    description: 'USER, ADMIN, DEV',
  })
  async generateAccessToken(@Body() params: any): Promise<string> {
    if (this._config.get<string>('NODE_ENV') === 'production') {
      throw new ImATeapotException(apiErrors.common.notAllowedInProduction);
    }
    try {
      const token = await this._authService.generateAccessToken(params);
      return token;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }
}
