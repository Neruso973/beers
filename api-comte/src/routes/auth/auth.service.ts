import {
  BadRequestException,
  ConflictException,
  Injectable,
} from '@nestjs/common';
import * as bcrypt from 'bcrypt';
import { UsersService } from '../users/users.service';
import {
  AccessTokenService,
  MailService,
  PrismaService,
  ReturnObject,
  apiErrors,
  apiSuccess,
  emailValidationTemplate,
  mailSubject,
  resetPasswordTemplate,
} from 'src/shared';
import {
  SignupUserDto,
  AuthDto,
  LoginUserDto,
  AuthBuilder,
  ResetPasswordDto,
  ValidateEmailDto,
} from './data';
import {
  CreateUserBuilder,
  CreateUserDto,
  RoleEnum,
  UserDto,
  modelToUserDto,
} from '../users/data';
import { ConfigService } from '@nestjs/config';

@Injectable()
export class AuthService {
  constructor(
    private readonly _userService: UsersService,
    private readonly _token: AccessTokenService,
    private readonly _mailService: MailService,
    private readonly _config: ConfigService,
    private readonly _prisma: PrismaService,
  ) {}

  async signup(
    signupUserDto: SignupUserDto,
    role: RoleEnum[],
  ): Promise<ReturnObject> {
    //verify user exists
    const userExists = await this._userService.findOne({
      where: { email: signupUserDto.email },
    });

    if (userExists) {
      throw new ConflictException(apiErrors.auth.userAlreadyExists);
    }

    //verify password
    if (signupUserDto.password !== signupUserDto.confirmPassword) {
      throw new BadRequestException(apiErrors.auth.passwordNotMatch);
    }

    //hash password
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(signupUserDto.password, salt);

    //create user
    const user: CreateUserDto = new CreateUserBuilder()
      .withEmail(signupUserDto.email)
      .withPassword(hashPassword)
      .withRole(role)
      .build();

    //save user
    const signupUser = await this._userService.create(user);

    if (!signupUser) {
      return null;
    }

    // send mail validation
    const result = this.sendVerifyEmail(signupUser.email);
    return result;
  }

  //sign in
  async signin(signinUserDto: LoginUserDto): Promise<AuthDto> {
    //verify user exists
    const userExists = await this._userService.findOne({
      where: { email: signinUserDto.email },
    });

    if (!userExists) {
      throw new BadRequestException(apiErrors.users.notFound);
    }

    //verify password
    const validPassword = await bcrypt.compare(
      signinUserDto.password,
      userExists.password,
    );

    if (!validPassword) {
      throw new BadRequestException(apiErrors.auth.invalidCredentials);
    }

    // view model
    delete userExists.password;
    delete userExists.resetPasswordToken;

    const isActive = userExists.isActive;

    if (!isActive) {
      throw new BadRequestException(apiErrors.users.notActive);
    }
    //create token
    const access_token = this._token.generateAccessToken(userExists);

    const authDto: AuthDto = new AuthBuilder()
      .withUser(userExists)
      .withAccessToken(access_token)
      .build();

    return authDto;
  }

  /**
   * @description send mail to user for reset password
   * @param {string}
   * @returns {ReturnObject}
   */
  async forgotPassword(email: string): Promise<ReturnObject> {
    //verify user exists
    const userExists = await this._userService.findOne({
      where: { email },
    });

    if (!userExists) {
      throw new BadRequestException(apiErrors.users.notFound);
    }

    //generate token
    const token = this._token.generateResetPasswordToken(userExists);

    //update user
    userExists.resetPasswordToken = token;
    const updateUser = await this._userService.update(
      userExists.id,
      userExists,
    );

    if (!updateUser) {
      throw new BadRequestException(apiErrors.users.notUpdated);
    }

    //send mail
    try {
      const resetPasswordUrl = `${this._config.get<string>(
        'BASE_URL',
      )}/reset-password?token=${updateUser.resetPasswordToken}`;
      const template = resetPasswordTemplate(
        updateUser,
        resetPasswordUrl,
        '24h',
      );
      const mailFrom = this._config.get<string>('MAIL_FROM');
      const mailTo = updateUser.email;

      const mail = {
        to: mailTo,
        subject: mailSubject.forgotPassword,
        from: mailFrom,
        html: template,
      };

      const sendMail = await this._mailService.send(mail);
      const response = sendMail[0];

      return {
        code: response.statusCode,
        message: apiSuccess.auth.forgotPassword,
      };
    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * function to reset user password by token
   * @param {ResetPasswordDto}
   * @returns {AuthDto}
   */
  async resetPassword(data: ResetPasswordDto): Promise<AuthDto> {
    //verify token
    const tokenExpired = this._token.isTokenValid(data.resetToken);

    if (tokenExpired) {
      throw new BadRequestException(apiErrors.auth.tokenExpired);
    }

    //verify user exists
    const userExists = await this._userService.findOne({
      where: { resetPasswordToken: data.resetToken },
    });

    if (!userExists) {
      throw new BadRequestException(apiErrors.users.notFound);
    }

    //verify password
    if (data.password !== data.confirmPassword) {
      throw new BadRequestException(apiErrors.auth.passwordNotMatch);
    }

    //hash password
    const salt = await bcrypt.genSalt();
    const hashPassword = await bcrypt.hash(data.password, salt);

    //update user
    userExists.password = hashPassword;
    userExists.resetPasswordToken = null;

    const updateUser = await this._userService.update(
      userExists.id,
      userExists,
    );

    if (!updateUser) {
      throw new BadRequestException(apiErrors.users.notUpdated);
    }

    //create token
    const returnUser = updateUser;
    delete returnUser.password;
    delete returnUser.resetPasswordToken;

    const access_token = this._token.generateAccessToken(userExists);

    const authDto: AuthDto = new AuthBuilder()
      .withUser(returnUser)
      .withAccessToken(access_token)
      .build();

    return authDto;
  }

  /**
   * @description verify email
   * @param data
   * @returns {AuthDto}
   */
  async verifyEmail(data: ValidateEmailDto): Promise<AuthDto> {
    //verify token
    const isTokenValid = this._token.isTokenValid(data.token);

    if (!isTokenValid) {
      throw new BadRequestException(apiErrors.auth.tokenExpired);
    }

    //verify user exists
    const userExists = await this._userService.findOne({
      where: { veifyMailToken: data.token },
    });

    if (!userExists) {
      throw new BadRequestException(apiErrors.users.notFound);
    }

    //update user
    userExists.veifyMailToken = null;
    userExists.isActive = true;

    const updateUser = await this._userService.update(
      userExists.id,
      userExists,
    );

    if (!updateUser) {
      throw new BadRequestException(apiErrors.users.notUpdated);
    }

    // generate access token
    const returnUser = updateUser;
    delete returnUser.password;
    delete returnUser.resetPasswordToken;

    const access_token = this._token.generateAccessToken(userExists);

    const authDto: AuthDto = new AuthBuilder()
      .withUser(returnUser)
      .withAccessToken(access_token)
      .build();

    return authDto;
  }

  /**
   * @description send or resend verify email
   * @param params
   * @returns {void}
   */
  async sendVerifyEmail(email: string): Promise<ReturnObject> {
    //verify user exists
    const userExists = await this._prisma.user.findUnique({
      where: { email },
    });

    if (!userExists) {
      throw new BadRequestException(apiErrors.users.notFound);
    }

    // get verify email token and check if is valid
    let verifyEmailToken = userExists.veifyMailToken;
    let isTokenValid = true;
    if (verifyEmailToken) {
      isTokenValid = this._token.isTokenValid(verifyEmailToken);
    }

    //check if token exists or is invalid
    if (!verifyEmailToken || !isTokenValid) {
      //generate new token
      verifyEmailToken = this.generateValidateEmailToken(email);

      //update user
      userExists.veifyMailToken = verifyEmailToken;
      await this._userService.update(userExists.id, modelToUserDto(userExists));
    }

    // send mail
    try {
      const validateEmailUrl = `${this._config.get<string>(
        'BASE_URL',
      )}/validate-email?token=${verifyEmailToken}`;
      const template = emailValidationTemplate(
        modelToUserDto(userExists),
        validateEmailUrl,
        '24h',
      );
      const mailFrom = this._config.get<string>('MAIL_FROM');
      const mailTo = email;

      const mail = {
        to: mailTo,
        subject: mailSubject.validateEmail,
        from: mailFrom,
        html: template,
      };

      const sendMail = await this._mailService.send(mail);
      const response = sendMail[0];

      return {
        code: response.statusCode,
        message: apiSuccess.auth.verifyEmail,
      };
    } catch (err) {
      throw new Error(err);
    }
  }

  /**
   * Function only for development with feature flipping
   * for generate access token with any role
   * @queryParam {string} where[role][has] - role of token
   * @returns {string} jwt token
   */
  async generateAccessToken(params): Promise<string> {
    const mokedUser: UserDto = {
      id: 0,
      email: 'dev@email.fr',
      role: [params.where.role.has],
      isActive: true,
    };

    const token = this._token.generateAccessToken(mokedUser);
    return token;
  }

  private generateValidateEmailToken(email: string): string {
    //generarte token
    const token = this._token.generateValidateEmailToken(email);
    return token;
  }
}
