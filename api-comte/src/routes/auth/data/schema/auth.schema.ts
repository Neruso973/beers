import * as Joi from 'joi';
import { joiPasswordExtendCore } from 'joi-password';
import { apiErrors } from '../../../../shared/utils/apiErrors';

export const signinSchema = Joi.object({
  email: Joi.string().email().max(255).required(),
  password: Joi.extend(joiPasswordExtendCore)
    .string()
    .min(8)
    .max(20)
    .minOfSpecialCharacters(1)
    .minOfLowercase(1)
    .minOfUppercase(1)
    .minOfNumeric(1)
    .noWhiteSpaces()
    .required(),
});

export const signupSchema = Joi.object({
  email: Joi.string().email().max(255).required(),
  password: Joi.extend(joiPasswordExtendCore)
    .string()
    .min(8)
    .max(20)
    .minOfSpecialCharacters(1)
    .minOfLowercase(1)
    .minOfUppercase(1)
    .minOfNumeric(1)
    .noWhiteSpaces()
    .required(),
  confirmPassword: Joi.string().valid(Joi.ref('password')).required().messages({
    'any.only': apiErrors.auth.passwordNotMatch,
  }),
});

export const forgotPasswordSchema = Joi.object({
  email: Joi.string().email().max(255).required(),
});

export const resetPasswordSchema = Joi.object({
  password: Joi.extend(joiPasswordExtendCore)
    .string()
    .min(8)
    .max(20)
    .minOfSpecialCharacters(1)
    .minOfLowercase(1)
    .minOfUppercase(1)
    .minOfNumeric(1)
    .noWhiteSpaces()
    .required(),
  confirmPassword: Joi.string().valid(Joi.ref('password')).required().messages({
    'any.only': apiErrors.auth.passwordNotMatch,
  }),
  resetToken: Joi.string().required(),
});
