import { UserDto } from "src/routes/users/data";

export class AuthDto {
    user: UserDto;
    access_token: string;
}