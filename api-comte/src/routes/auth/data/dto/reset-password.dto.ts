export class ResetPasswordDto {
    password: string;
    confirmPassword: string;
    resetToken: string;
}