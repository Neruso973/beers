import { UserDto } from "src/routes/users/data";
import { AuthDto } from "./auth.dto";

export class AuthBuilder {
    private auth: AuthDto;

    constructor() {
        this.auth = new AuthDto();
    }

    withUser(user: UserDto) {
        this.auth.user = user;
        return this;
    }

    withAccessToken(access_token: string) {
        this.auth.access_token = access_token;
        return this;
    }

    build() {
        return this.auth;
    }
}