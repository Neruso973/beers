import { MapperService } from "src/shared";
import { CreateBeerDto } from "../dto/create-beer.dto";
import { CreateBeer } from "../model/create-beer";
import { BeerDto } from "../dto/beer.dto";
import { Beer } from "../model/beer";

// Fonctions spécifiques de conversion
export function dtoToCreateBeerModel(
    createBeerDto: CreateBeerDto
  ): CreateBeer {
    return MapperService.convertObject(createBeerDto, CreateBeer);
  }
  
  export function dtoToBeerModel(beerDto: BeerDto): Beer {
    return MapperService.convertObject(beerDto, Beer);
  }
  
  export function modelToBeerDto(prismaModel: any): BeerDto {
    return MapperService.convertObject(prismaModel, BeerDto);
  }