import * as Joi from 'joi';

export const beerSchema = Joi.object({
  id: Joi.number().integer().required(),
  name: Joi.string().email().max(255).required(),
  description: Joi.string().max(255),
});
