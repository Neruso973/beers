import { Prisma } from '@prisma/client';
import { CreateBeer } from './create-beer';

interface BeerInterface extends Prisma.BeerUpdateInput {}

export class Beer extends CreateBeer implements BeerInterface {
  id: number;
}
