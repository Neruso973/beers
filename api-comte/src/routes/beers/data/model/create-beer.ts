import { Prisma } from '@prisma/client';

interface BeerInterface extends Prisma.BeerCreateInput {}

export class CreateBeer implements BeerInterface {
  name: string;
  description: string;
  created_at: Date;
  bottled_at?: Date;
  hops?: Prisma.HopCreateNestedManyWithoutBeersInput;
  malts?: Prisma.MaltCreateNestedManyWithoutBeersInput;
}
