import { CreateHopDto } from "src/routes/hop/dto/create-hop.dto";
import { CreateMaltDto } from "src/routes/malt/dto/create-malt.dto";

export class CreateBeerDto {
    name: string;
    description?: string;
    malts: CreateMaltDto[];
    hops: CreateHopDto[];
}
