import { PartialType } from '@nestjs/swagger';
import { CreateBeerDto } from './create-beer.dto';

export class BeerDto extends PartialType(CreateBeerDto) {
    id: number;
}
