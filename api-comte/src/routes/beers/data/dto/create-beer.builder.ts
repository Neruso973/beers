import { CreateMaltDto } from "src/routes/malt/dto/create-malt.dto";
import { CreateBeerDto } from "./create-beer.dto";
import { CreateHopDto } from "src/routes/hop/dto/create-hop.dto";

export class CreateBeerBuilder {
    private beer: CreateBeerDto;
    
    constructor() {
        this.beer = new CreateBeerDto();
    }
    public withName(name: string): CreateBeerBuilder {
        this.beer.name = name;
        return this;
    }
    public withDescription(description: string): CreateBeerBuilder {
        this.beer.description = description;
        return this;
    }
    public withMalts(malts: CreateMaltDto[]): CreateBeerBuilder {
        this.beer.malts = malts;
        return this;
    }
    public withHops(hops: CreateHopDto[]): CreateBeerBuilder {
        this.beer.hops = hops;
        return this;
    }
    public build(): CreateBeerDto {
        return this.beer;
    }
}