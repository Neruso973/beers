import {
  Controller,
  Get,
  Post,
  Body,
  Patch,
  Param,
  Delete,
  NotFoundException,
  UsePipes,
} from '@nestjs/common';
import { BeersService } from './beers.service';
import { CreateBeerDto } from './data/dto/create-beer.dto';
import { BeerDto } from './data';
import { ApiTags } from '@nestjs/swagger';
import { LogService } from '../log/log.service';
import { CategoryEnum } from '../log/data/dto/create-log.dto';
import { ParamToNumberPipe, PrismaQueryParams, apiErrors } from 'src/shared';

@ApiTags('api/beers')
@Controller('beers')
export class BeersController {
  constructor(
    private readonly _beersService: BeersService,
    private readonly _log: LogService,
  ) {}

  @Post()
  async create(@Body() createBeerDto: CreateBeerDto): Promise<BeerDto> {
    try {
      const newBeer = await this._beersService.create(createBeerDto);
      return newBeer;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Get()
  async findAll(@Body() body: PrismaQueryParams<BeerDto>): Promise<BeerDto[]>{
    try {
      const beers = await this._beersService.findAll(body);
      return beers;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Get(':id')
  async findOne(@Body() body: PrismaQueryParams<BeerDto>): Promise<BeerDto> {
    try {
      const beer = await this._beersService.findOne(body);
      if (!beer) {
        throw new NotFoundException(apiErrors.beers.notFound);
      }
      return beer;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Patch(':id')
  @UsePipes(ParamToNumberPipe)
  async update(@Param('id') id: number, @Body() beer: BeerDto) {
    try {
      const updatedUser = await this._beersService.update(id, beer);
      return updatedUser;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Delete(':id')
  @UsePipes(ParamToNumberPipe)
  async remove(@Param('id') id: number) {
    try {
      const deletedUser = await this._beersService.remove(id);
      return deletedUser;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }
}
