import { Module } from '@nestjs/common';
import { BeersService } from './beers.service';
import { BeersController } from './beers.controller';
import { SharedModule } from 'src/shared/shared.module';

@Module({
  controllers: [BeersController],
  providers: [BeersService],
  imports: [SharedModule],
})
export class BeersModule {}
