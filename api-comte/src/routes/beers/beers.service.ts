import { BadRequestException, Injectable, NotFoundException } from '@nestjs/common';
import { CreateBeerDto } from './data/dto/create-beer.dto';
import { BeerDto } from './data/dto/beer.dto';
import { PrismaQueryParams, PrismaService, apiErrors } from 'src/shared';
import { Beer, CreateBeer, dtoToBeerModel, dtoToCreateBeerModel, modelToBeerDto } from './data';

@Injectable()
export class BeersService {
  constructor(private readonly _prisma: PrismaService) {}
  
  async create(createBeerDto: CreateBeerDto): Promise<BeerDto> {
    const createBeer: CreateBeer = dtoToCreateBeerModel(createBeerDto);
    const createdBeer = await this._prisma.beer.create({ data: createBeer });
    if (!createdBeer) {
      throw new BadRequestException(apiErrors.beers.notCreated);
    }
    return modelToBeerDto(createdBeer);
  }

  async findAll(body: PrismaQueryParams<BeerDto>): Promise<BeerDto[]> {
    const beers = await this._prisma.beer.findMany(body as any);
    if (!beers.length) {
      throw new NotFoundException(apiErrors.beers.notFound);
    }

    const result: BeerDto[] = beers.map((beer) => modelToBeerDto(beer));

    return result;
  }

  async findOne(body: PrismaQueryParams<BeerDto>): Promise<BeerDto> {
    const beer = await this._prisma.beer.findUnique(body as any);
    if (!beer) {
      return null;
    }

    const beerDto = modelToBeerDto(beer);

    return beerDto;
  }

  async update(id: number, updateBeerDto: BeerDto): Promise<BeerDto> {
    //TODO: check if current user is owner of the beer with currentBeerInterceptior

    const beerToUpdate = await this.findOne({where: {id}});
    if(!beerToUpdate) {
      throw new NotFoundException(apiErrors.beers.notFoundToUpdate);
    }

    const updateBeer: Beer = dtoToBeerModel(updateBeerDto);

    const updatedBeer = await this._prisma.beer.update({
      where: { id },
      data: updateBeer,
    });

    if (!updatedBeer) {
      throw new BadRequestException(apiErrors.beers.notUpdated);
    }

    return modelToBeerDto(updatedBeer);
  }

  async remove(id: number): Promise<BeerDto> {
    const beerToDelete = await this.findOne({where: {id}});
    if(!beerToDelete) {
      throw new NotFoundException(apiErrors.beers.notFoundToDelete);
    }

    const deletedBeer = await this._prisma.beer.delete({where: {id}});
    if(!deletedBeer) {
      throw new BadRequestException(apiErrors.beers.notDeleted);
    }

    return modelToBeerDto(deletedBeer);
  }
}
