export class CreateUserDto {
  email: string;
  password: string;
  created_at: Date;
  role: RoleEnum[];
}

export enum RoleEnum {
  ADMIN = 'ADMIN',
  USER = 'USER',
  COMTE_DEV = 'COMTE_DEV',
  DEV = 'DEV',
  DATA = 'DATA',
}
