import { PartialType } from '@nestjs/swagger';
import { CreateUserDto } from './create-user.dto';
import { Address } from 'src/shared';

export class UserDto extends PartialType(CreateUserDto) {
    id: number;
    firstname?: string;
    lastname?: string;
    username?: string;
    birth?: Date;
    address?: Address
    resetPasswordToken?:   string;
    isActive: boolean;
    veifyMailToken?: string;
}
