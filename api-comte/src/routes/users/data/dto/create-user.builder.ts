import { CreateUserDto, RoleEnum } from "./create-user.dto";

export class CreateUserBuilder {
    private createUser;

    constructor() {
        this.createUser = new CreateUserDto();
    }

    withEmail(email: string) {
        this.createUser.email = email;
        return this;
    }

    withPassword(password: string) {
        this.createUser.password = password;
        return this;
    }

    withRole(role: RoleEnum[]) {
        this.createUser.role = role;
        return this;
    }

    build() {
        return this.createUser;
    }
}