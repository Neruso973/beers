import { MapperService } from 'src/shared/services/mapper.service';
import { CreateUserDto } from '../dto/create-user.dto';
import { User } from '../model/user';
import { UserDto } from '../dto/user.dto';
import { CreateUser } from '../model/create-user';

// Fonctions spécifiques de conversion
export function dtoToCreateUserModel(
  createUserDto: CreateUserDto
): CreateUser {
  return MapperService.convertObject(createUserDto, CreateUser);
}

export function dtoToUserModel(userDto: UserDto): User {
  return MapperService.convertObject(userDto, User);
}

export function modelToUserDto(prismaModel: any): UserDto {
  return MapperService.convertObject(prismaModel, UserDto);
}