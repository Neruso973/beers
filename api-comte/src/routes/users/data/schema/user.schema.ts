import * as Joi from 'joi';
import { addressSchema } from './address.schema';

export const userSchema = Joi.object({
  id: Joi.number().integer().required(),
  email: Joi.string().email().max(255).required(),
  username: Joi.string().max(255),
  firstname: Joi.string().max(255),
  lastname: Joi.string().max(255),
  birth: Joi.date(),
  created_at: Joi.date(),
  role: Joi.array().items(Joi.string().valid('ADMIN', 'USER', 'DEV', 'COMTE_DEV')),
  address: Joi.object().allow(addressSchema)
});
