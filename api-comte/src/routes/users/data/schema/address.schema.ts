import * as Joi from 'joi';

export const addressSchema = Joi.object({
  id: Joi.number().integer().required(),
  address: Joi.string().max(255).required(),
  addressComplement: Joi.string().max(255),
  zipcode: Joi.string().max(255).required(),
  city: Joi.string().max(255).required(),
  country: Joi.string().max(255).required(),
});
