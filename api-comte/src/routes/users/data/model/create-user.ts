import { Prisma, RoleEnum } from '@prisma/client';

interface UserInterface extends Prisma.UserCreateInput {}

export class CreateUser implements UserInterface {
  email: string;
  password: string;
  firstname: string;
  lastname: string;
  username: string;
  role: RoleEnum[];
  birth: Date;
  created_at: Date;
}
