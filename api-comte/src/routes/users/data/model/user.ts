import { Prisma } from '@prisma/client';
import { CreateUser } from './create-user';

interface UserInterface extends Prisma.UserUpdateInput {}

export class User extends CreateUser implements UserInterface {
  id: number;
  resetPasswordToken?: string | Prisma.NullableStringFieldUpdateOperationsInput;
}
