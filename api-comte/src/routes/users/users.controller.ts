import {
  Controller,
  Get,
  Body,
  Patch,
  Param,
  Delete,
  UsePipes,
  NotFoundException,
  UseGuards,
  Post,
} from '@nestjs/common';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';
import { UsersService } from './users.service';
import { LogService } from '../log/log.service';
import { CategoryEnum } from '../log/data/dto/create-log.dto';
import { CreateUserDto, RoleEnum, UserDto } from './data';
import {
  AuthorizeRoles,
  JwtAuthGuard,
  ParamToNumberPipe,
  PrismaQueryParams,
  RoleAuthGuard,
  apiErrors,
} from 'src/shared';

@ApiTags('api/users')
@ApiBearerAuth('jwt')
@Controller('api/users')
export class UsersController {
  constructor(
    private readonly _usersService: UsersService,
    private readonly _log: LogService,
  ) {}

  @Post()
  @AuthorizeRoles(RoleEnum.ADMIN)
  @UseGuards(RoleAuthGuard)
  async create(@Body() createUserDto: CreateUserDto): Promise<UserDto> {
    try {
      const newUser = await this._usersService.create(createUserDto);
      return newUser;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Get()
  @UseGuards(JwtAuthGuard)
  async findAll(@Body() body: PrismaQueryParams<UserDto>): Promise<UserDto[]> {
    try {
      const users = await this._usersService.findAll(body);
      return users;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Get(':id')
  @UseGuards(JwtAuthGuard)
  async findOne(@Body() body: PrismaQueryParams<UserDto>): Promise<UserDto> {
    try {
      const user = await this._usersService.findOne(body);
      if (!user) {
        throw new NotFoundException(apiErrors.users.notFound);
      }
      return user;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Patch(':id')
  @UseGuards(JwtAuthGuard)
  @UsePipes(ParamToNumberPipe)
  async update(@Param('id') id: number, @Body() user: UserDto) {
    try {
      const updatedUser = await this._usersService.update(id, user);
      return updatedUser;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  @Delete(':id')
  @UseGuards(JwtAuthGuard)
  @UsePipes(ParamToNumberPipe)
  async remove(@Param('id') id: number) {
    try {
      const deletedUser = await this._usersService.remove(id);
      return deletedUser;
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }
}
