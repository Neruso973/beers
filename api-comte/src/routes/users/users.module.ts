import { Module } from '@nestjs/common';
import { UsersService } from './users.service';
import { UsersController } from './users.controller';
import { SharedModule } from 'src/shared/shared.module';
import { AuthController } from '../auth/auth.controller';
import { AuthService } from '../auth/auth.service';

@Module({
  controllers: [UsersController, AuthController],
  providers: [UsersService, AuthService],
  imports: [SharedModule],
  exports: [UsersService],
})
export class UsersModule {}
