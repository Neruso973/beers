import {
  BadRequestException,
  Injectable,
  NotFoundException,
  UnauthorizedException,
} from '@nestjs/common';
import { PrismaQueryParams, PrismaService, apiErrors } from 'src/shared';
import {
  CreateUser,
  CreateUserDto,
  User,
  UserDto,
  dtoToCreateUserModel,
  dtoToUserModel,
  modelToUserDto,
} from './data';

@Injectable()
export class UsersService {
  constructor(private readonly _prisma: PrismaService) {}

  async create(createUserDto: CreateUserDto): Promise<UserDto> {
    const emailExists = await this._prisma.user.findUnique({where: {email: createUserDto.email}});
    if(emailExists) {
      throw new BadRequestException(apiErrors.auth.userAlreadyExists);
    }
    
    const createUser: CreateUser = dtoToCreateUserModel(createUserDto);
    const createdUser = await this._prisma.user.create({ data: createUser });
    if (!createdUser) {
      throw new BadRequestException(apiErrors.users.notCreated);
    }
    return modelToUserDto(createdUser);
  }

  async findAll(body: PrismaQueryParams<UserDto>): Promise<UserDto[]> {
    const users = await this._prisma.user.findMany(body as any);
    if (!users.length) {
      throw new NotFoundException(apiErrors.users.notFound);
    }

    const result: UserDto[] = users.map((user) => modelToUserDto(user));

    return result;
  }

  async findOne(body: PrismaQueryParams<UserDto>): Promise<UserDto> {
    const user = await this._prisma.user.findUnique(body as any);
    if (!user) {
      return null;
    }

    const userDto = modelToUserDto(user);

    return userDto;
  }

  async update(id: number, updateUserDto: UserDto): Promise<UserDto> {
    if(id !== updateUserDto.id) {
      throw new UnauthorizedException(apiErrors.users.unauthorized);
    }

    const userToUpdate = await this.findOne({where: {id}});
    if(!userToUpdate) {
      throw new NotFoundException(apiErrors.users.notFoundToUpdate);
    }

    const updateUser: User = dtoToUserModel(updateUserDto);

    const updatedUser = await this._prisma.user.update({
      where: { id },
      data: updateUser,
    });

    if (!updatedUser) {
      throw new BadRequestException(apiErrors.users.notUpdated);
    }

    return modelToUserDto(updatedUser);
  }

  async remove(id: number): Promise<UserDto> {
    const userToDelete = await this.findOne({where: {id}});
    if(!userToDelete) {
      throw new NotFoundException(apiErrors.users.notFoundToDelete);
    }

    const deletedUser = await this._prisma.user.delete({where: {id}});
    if(!deletedUser) {
      throw new BadRequestException(apiErrors.users.notDeleted);
    }

    return modelToUserDto(deletedUser);
  }
}
