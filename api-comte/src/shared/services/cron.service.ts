import { Injectable } from '@nestjs/common';
import { Cron, CronExpression } from '@nestjs/schedule';
import { PrismaService } from './prisma.service';
import { AccessTokenService } from './access-token.service';
import { LogService } from 'src/routes/log/log.service';
import { CategoryEnum } from 'src/routes/log/data/dto/create-log.dto';
import { ConfigService } from '@nestjs/config';
import { pgDump } from 'pg-dump-restore';
import { DateHelperService } from './date-helper.service';
import * as util from 'util';
import * as fs from 'fs';
import { PassThrough } from 'stream';

@Injectable()
export class CronService {
  constructor(
    private readonly _prisma: PrismaService,
    private readonly _token: AccessTokenService,
    private readonly _log: LogService,
    private readonly _config: ConfigService,
  ) {}

  /**
   * every day at 3am
   * @description Remove all expired token from users
   * @returns {Promise<void>}
   */
  @Cron(CronExpression.EVERY_DAY_AT_3AM)
  async removeExpiredToken(): Promise<void> {
    try {
      const users = await this._prisma.user.findMany();
      const expiredResetPasswordToken: string[] = [];
      const expiredVerifyEmailToken: string[] = [];

      //get all invalid token from users
      users.forEach((user) => {
        //check if resetPasswordToken is valid
        if (user.resetPasswordToken) {
          const isExpired = !this._token.isTokenValid(user.resetPasswordToken);
          if (isExpired) {
            expiredResetPasswordToken.push(user.resetPasswordToken);
          }
        }

        //check if verifyEmailToken is valid
        if (user.veifyMailToken) {
          const isExpired = !this._token.isTokenValid(user.veifyMailToken);
          if (isExpired) {
            expiredVerifyEmailToken.push(user.veifyMailToken);
          }
        }
      });

      //remove all invalid reset password tokens
      if (expiredResetPasswordToken.length > 0) {
        await this._prisma.$transaction(async (prisma: PrismaService) => {
          for (let i = 0; i < expiredResetPasswordToken.length; i++) {
            await prisma.user.update({
              where: {
                resetPasswordToken: expiredResetPasswordToken[i],
              },
              data: {
                resetPasswordToken: null,
              },
            });
          }
          console.log('expired resret password token removed');
        });
      } else {
        console.log('no expired reset password token');
      }

      //remove all invalid reset password tokens
      if (expiredVerifyEmailToken.length > 0) {
        await this._prisma.$transaction(async (prisma: PrismaService) => {
          for (let i = 0; i < expiredVerifyEmailToken.length; i++) {
            await prisma.user.update({
              where: {
                veifyMailToken: expiredVerifyEmailToken[i],
              },
              data: {
                veifyMailToken: null,
              },
            });
          }
          console.log('expired valid email token removed');
        });
      } else {
        console.log('no valid email expired token');
      }
    } catch (err) {
      console.error(err);
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      throw new Error('Erreur lors de la transaction Prisma');
    }
  }
  catch(err) {
    this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
    throw new Error();
  }

  /**
   * every week
   * @description Create a database dump
   */
  @Cron(CronExpression.EVERY_WEEK)
  async createDatabaseDump() {
    const writeFileAsync = util.promisify(fs.writeFile);

    const databaseHost = this._config.get<string>('DATABASE_HOST');
    const databasePort = this._config.get<string>('DATABASE_PORT');
    const databaseUser = this._config.get<string>('DATABASE_USER');
    const databaseName = this._config.get<string>('DATABASE_NAME');
    const databasePassword = this._config.get<string>('DATABASE_PASSWORD');

    const dumpFileName = `./public/dump/dump_${DateHelperService.getFormatedDate(
      true,
    )}.sql`;

    const dump: any = await pgDump(
      {
        port: +databasePort,
        host: databaseHost,
        database: databaseName,
        username: databaseUser,
        password: databasePassword,
      },
      {
        filePath: dumpFileName,
      },
    );

    // Create a PassThrough stream to capture the output
    const outputBuffer = new PassThrough();

    // Wait for the dump process to complete
    await dump;

    // Read the content of the buffer stream
    const dumpBuffer: Buffer[] = [];
    outputBuffer.on('data', (chunk) => dumpBuffer.push(chunk));

    // Wait for the 'end' event to ensure the buffer is complete
    await new Promise((resolve) => outputBuffer.on('end', resolve));

    // Concatenate the chunks into a single Buffer
    const dumpData = Buffer.concat(dumpBuffer);

    // Write the dump to a file asynchronously
    await writeFileAsync(dumpFileName, dumpData);

    this._log.log(
      null,
      this.constructor.name,
      CategoryEnum.ERROR,
      `Database dump created at: ${dumpFileName}`,
    );
  }

  /**
   * every month
   * @description Remove all dump older than 1 month
   */
  @Cron(CronExpression.EVERY_1ST_DAY_OF_MONTH_AT_MIDNIGHT)
  removeOldDump() {
    const path = require('path');
    const dumpFolder = './public/dump';

    fs.readdir(dumpFolder, (err, files) => {
      if (err) throw err;

      files.forEach((file) => {
        const filePath = path.join(dumpFolder, file);
        const fileDate = DateHelperService.getDateByFilename(file);

        if (fileDate) {
          const dif = DateHelperService.getDifferenceInMonth(fileDate);
          if (dif > 1) {
            fs.unlink(filePath, (err) => {
              if (err) {
                this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
                throw err;
              }
              this._log.log(
                err,
                this.constructor.name,
                CategoryEnum.INFO,
                `File ${file} deleted`,
              );
            });
          }
        }
      });
    });
  }
}
