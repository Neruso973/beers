import { Injectable } from '@nestjs/common';
import { PrismaQueryParams } from '../index';

@Injectable()
export class QueryParamsService<T> {
  //http://localhost:8080/honey?where[id]=4&where[AND][isQuotation]=false&where[AND][toto]=true&include[list]&include[toto]&orderBy[smallPrice]=desc
  private _prismaQueryParams: PrismaQueryParams<T>;

  public getPrismaQueryParams(): PrismaQueryParams<T> {
    return this._prismaQueryParams;
  }

  public setPrismaQueryParams(prismaQueryParams: PrismaQueryParams<T>) {
    this._prismaQueryParams = prismaQueryParams;
  }

  /**
   * @description format query params form request for prisma call
   * @param queryParams 
   * @returns 
   */
  public formatQueryParams(queryParams: any): PrismaQueryParams<T> {
    //change string to number
    let prismaQueryParams: PrismaQueryParams<T> = queryParams;

    if (prismaQueryParams.include) {
      prismaQueryParams.include = this._formatIncludesProperty(
        prismaQueryParams.include,
      );
    }

    prismaQueryParams = this._convertStringBoolean(prismaQueryParams);
    prismaQueryParams = this._convertStringsToNumbers(prismaQueryParams);
    return prismaQueryParams;
  }

  /**
   * @description convert all number-string of any object to number
   * @param obj 
   * @returns 
   */
  private _convertStringsToNumbers(obj: any): any {
    if (typeof obj === 'string' && !isNaN(Number(obj))) {
      return Number(obj);
    } else if (typeof obj === 'object' && obj !== null) {
      for (const key in obj) {
        if (obj.hasOwnProperty(key)) {
          obj[key] = this._convertStringsToNumbers(obj[key]);
        }
      }
    }
    return obj;
  }

  /**
   * @description format include property
   * @param obj 
   * @returns 
   */
  private _formatIncludesProperty(obj: any) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (obj[key] === '') {
          obj[key] = true;
        }
      }
    }
    return obj;
  }

  /**
   * @description convert string boolean of object to boolean
   * @param obj 
   * @returns 
   */
  private _convertStringBoolean(obj) {
    for (const key in obj) {
      if (obj.hasOwnProperty(key)) {
        if (typeof obj[key] === 'object' && obj[key] !== null) {
          this._convertStringBoolean(obj[key]);
        } else if (typeof obj[key] === 'string') {
          if (obj[key].toLowerCase() === 'true') {
            obj[key] = true;
          } else if (obj[key].toLowerCase() === 'false') {
            obj[key] = false;
          }
        }
      }
    }
    return obj;
  }
}
