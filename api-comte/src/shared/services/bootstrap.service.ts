import { Injectable, OnModuleInit } from '@nestjs/common';
import { PrismaService } from './prisma.service';
import { ConfigService } from '@nestjs/config';
import * as bcrypt from 'bcrypt';
import { RoleEnum } from '@prisma/client';
import { LogService } from 'src/routes/log/log.service';
import { CategoryEnum } from 'src/routes/log/data/dto/create-log.dto';
import { apiSuccess } from '../utils/apiSuccess';

@Injectable()
export class BootstrapService implements OnModuleInit {
  constructor(
    private readonly _prisma: PrismaService,
    private readonly _config: ConfigService,
    private readonly _log: LogService,
  ) {}

  isProduction = this._config.get<string>('NODE_ENV') === 'production';

  /**
   * @description function called when module is initialized
   * @returns {void}
   */
  async onModuleInit() {
    await this.checkAndCreateAdmin();
    if (!this.isProduction) {
      await this.createDevAccount();
    }
  }

  /**
   * @description create default admin if not exist
   * @returns {void}
   */
  private async checkAndCreateAdmin() {
    try {
      const adminCount = await this._prisma.user.count({
        where: { role: { has: RoleEnum.ADMIN } },
      });

      if (adminCount === 0) {
        this._log.log(
          null,
          this.constructor.name,
          CategoryEnum.INFO,
          apiSuccess.auth.createDefaultAdmin,
        );

        // hash password
        const salt = await bcrypt.genSalt();
        const hashPassword = await bcrypt.hash(
          this._config.get<string>('ADMIN_PASSWORD'),
          salt,
        );

        // create admin
        await this._prisma.user.create({
          data: {
            email: this._config.get<string>('ADMIN_EMAIL'),
            password: hashPassword,
            role: [RoleEnum.ADMIN],
          },
        });
      }
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }

  /**
   * @description create default dev account if not exist
   * @returns {void}
   */
  async createDevAccount() {
    try {
      const devCount = await this._prisma.user.count({
        where: { email: this._config.get<string>('DEV_EMAIL') },
      });

      if (devCount === 0) {
        this._log.log(
          null,
          this.constructor.name,
          CategoryEnum.INFO,
          apiSuccess.auth.createDevelopementAccount,
        );

        // hash password
        const salt = await bcrypt.genSalt();
        const hashPassword = await bcrypt.hash(
          this._config.get<string>('DEV_PASSWORD'),
          salt,
        );

        // create dev
        const dev = await this._prisma.user.create({
          data: {
            email: this._config.get<string>('DEV_EMAIL'),
            password: hashPassword,
            role: [RoleEnum.ADMIN],
          },
        });

        // activate dev account
        dev.isActive = true;
        await this._prisma.user.update({
          where: { id: dev.id },
          data: dev,
        });
      }
    } catch (err) {
      this._log.log(err, this.constructor.name, CategoryEnum.ERROR);
      return err;
    }
  }
}
