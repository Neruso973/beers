import { Injectable } from '@nestjs/common';
import { UserDto } from 'src/routes/users/data';
import { JwtService } from '@nestjs/jwt';
import { ConfigService } from '@nestjs/config';
import { DateHelperService } from './date-helper.service';

@Injectable()
export class AccessTokenService {
  constructor(
    private readonly _jwt: JwtService,
    private readonly _config: ConfigService,
  ) {}

  secretKey: string = this._config.get<string>('SECRET_KEY');

  /**
   * @description generate auth token with user data
   * @param {UserDto}
   * @returns {string} JWT
   */
  public generateAccessToken(userData: UserDto): string {
    //construct payload
    const payload = {
      id: userData.id,
      email: userData.email,
      role: userData.role,
      iat: DateHelperService.getTodayTime(),
      exp: DateHelperService.getExpirationTime(8),
    };

    //generarte token
    const token = this._jwt.sign(payload, {
      secret: this.secretKey,
    });

    // return token
    return token;
  }

  /**
   * @description get current user with token from request
   * @param {string} JWT
   * @returns {payload}
   */
  public getCurrentUser(token: string) {
    const user = this._jwt.verify(token, { secret: this.secretKey });
    return user;
  }

  /**
   * @description check if auth token is valid
   * @param {string} JWT
   * @returns {boolean}
   */
  public isTokenValid(token: string): boolean {
    const decoded = this._jwt.verify(token, { secret: this.secretKey });
    const exp = decoded.exp;
    return !DateHelperService.isTokenExpired(exp);
  }

  /**
   * @description generate reset password token with user data
   * valid for 24h
   * @param {UserDto}
   * @returns {string} JWT
   */
  public generateResetPasswordToken(userData: UserDto): string {
    //construct payload
    const payload = {
      id: userData.id,
      email: userData.email,
      iat: DateHelperService.getTodayTime(),
      exp: DateHelperService.getTomorrowTime(),
    };

    //generarte token
    const token = this._jwt.sign(payload, {
      secret: this.secretKey,
    });

    // return token
    return token;
  }
  
    /**
     * @description generate validate email token 
     * @param {email}
     * @returns {string} JWT
     */
    public generateValidateEmailToken(email: string): string {
      //construct payload
      const payload = {
        email,
        iat: DateHelperService.getTodayTime(),
        exp: DateHelperService.getTodayTime(),
      };
  
      //generarte token
      const token = this._jwt.sign(payload, {
        secret: this.secretKey,
      });
  
      // return token
      return token;
    }

  /**
   * @description check if token in db is already valid
   * @param {string} JWT
   * @returns {boolean}
   */
  public isResetTokenValid(token: string): boolean {
    const decoded = this._jwt.verify(token, { secret: this.secretKey });
    const exp = decoded.exp;
    return !DateHelperService.isTokenExpired(exp);
  }
}
