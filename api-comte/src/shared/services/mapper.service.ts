import { Injectable } from '@nestjs/common';

@Injectable()
export class MapperService {
  
  /**
   * @description convert object to another object
   * @param source type
   * @returns destination typeÒ
   */
  public static convertObject<TSource, TDestination>(
    source: TSource,
    destination: new () => TDestination,
  ): TDestination {
    const result = new destination();
    Object.assign(result, source);
    return result;
  }
}
