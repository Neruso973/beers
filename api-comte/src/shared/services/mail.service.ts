import { Injectable } from '@nestjs/common';
import { LogService } from '../../routes/log/log.service';
import * as SendGrid from '@sendgrid/mail';
import { ConfigService } from '@nestjs/config';
import { CategoryEnum } from 'src/routes/log/data/dto/create-log.dto';

@Injectable()
export class MailService {
  constructor(
    private readonly _config: ConfigService,
    private _log: LogService,
  ) {
    SendGrid.setApiKey(this._config.get<string>('SEND_GRID_KEY'));
  }

  /**
   * @description send mail
   * @param {SendGrid.MailDataRequired} mail
   * @returns {Promise<SendGrid.MailService>} sendgrid transport response
   */
  async send(mail: SendGrid.MailDataRequired) {
    const transport = await SendGrid.send(mail);
    this._log.log(
      null,
      this.constructor.name,
      CategoryEnum.INFO,
      `E-Mail sent to ${mail.to}`,
    );
    return transport;
  }
}
