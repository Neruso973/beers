import { Injectable } from '@nestjs/common';
import { REGEX } from '../utils/constants';

@Injectable()
export class DateHelperService {
  /**
   * @description get today date in timestamp
   * @returns {number} today
   */
  public static getTodayTime(): number {
    const today = new Date();
    return today.getTime();
  }

  /**
   * @description get tomorow date
   * @returns {Date} tomorow
   */
  public static getTomorrow(): Date {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return tomorrow;
  }

  /**
   * @description get tomorow date in timestamp
   * @returns {number} tomorow
   */
  public static getTomorrowTime(): number {
    const today = new Date();
    const tomorrow = new Date(today);
    tomorrow.setDate(tomorrow.getDate() + 1);
    return tomorrow.getTime();
  }

  /**
   * @description get expiration date in X hour
   * @param {number} hour
   * @returns {Date}
   */
  public static getExpiration(hour: number): Date {
    const today = new Date();
    const expiration = new Date(today);
    expiration.setHours(expiration.getHours() + hour);
    return expiration;
  }

  /**
   * @description get expiration date in X hour in timestamp
   * @param {number} hour
   * @returns {number}
   */
  public static getExpirationTime(hour: number): number {
    const today = new Date();
    const expiration = new Date(today);
    expiration.setHours(expiration.getHours() + hour);
    return expiration.getTime();
  }

  /**
   * @description check if token is expired
   * @param {number} expirationTime
   * @returns {boolean}
   */
  public static isTokenExpired(expirationTime: number): boolean {
    const now = new Date();
    const expiration = new Date(expirationTime);

    return now.getTime() > expiration.getTime();
  }

  /**
   * @description return formated dd-mm-yyyy date
   * @returns {string}
   */
  public static getFormatedDate(withHour: boolean = false): string {
    const today = new Date();
    const dd = String(today.getDate()).padStart(2, '0');
    const mm = String(today.getMonth() + 1).padStart(2, '0');
    const yyyy = today.getFullYear();
    const hour = String(today.getHours()).padStart(2, '0');
    const minutes = String(today.getMinutes()).padStart(2, '0');

    if (withHour) {
      return `${dd}-${mm}-${yyyy}_${hour}:${minutes}`;
    } else {
      return `${dd}-${mm}-${yyyy}`;
    }
  }

  /**
   * @description get date by filename
   * @returns {Date}
   */
  public static getDateByFilename(filename: string): Date {
    const dateRegex = REGEX.DATENAME;
    const match = filename.match(dateRegex);
    if (match) {
      const day = match[1];
      const month = match[2];
      const year = match[3];
      const hour = match[4];
      const minute = match[5];

      // Création d'un objet Date
      const fileDate = new Date(
        `${year}-${month}-${day}T${hour}:${minute}:00Z`,
      );
      return fileDate;
    }
  }

  /**
   * @description return difference between now and a date in month
   * @param {Date} date
   */
  public static getDifferenceInMonth(date: Date): number {
    const today = new Date();
    const diff = today.getTime() - date.getTime();
    return Math.floor(diff / (1000 * 60 * 60 * 24 * 30));
  }
}
