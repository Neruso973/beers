import { UserDto } from "src/routes/users/data";

export const emailValidationTemplate = (user: UserDto, validationUrl: string, expirationTime: string): string => {
    return `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Validation d'e-mail</title>
      </head>
      <body style="font-family: Arial, sans-serif;">
  
        ${user.firstname && user.lastname ? `<p>Bonjour ${user.firstname} ${user.lastname},</p>` : `<p>Bonjour,</p>`}
  
        <p>Merci de vous être inscrit à La Comté du dev. Veuillez cliquer sur le lien ci-dessous pour valider votre adresse e-mail :</p>
  
        <p><a href="${validationUrl}">Valider l'e-mail</a></p>
  
        <p>Le lien expirera dans ${expirationTime}.</p>
  
        <p>Merci,<br>La Comté du dev</p>
  
      </body>
      </html>
    `;
};