export const mailSubject = {
  forgotPassword: 'Réinitialisation de votre mot de passe',
  validateEmail: 'Validation de votre adresse email',
};
