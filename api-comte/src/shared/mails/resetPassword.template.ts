import { UserDto } from "src/routes/users/data";

export const resetPasswordTemplate = (user: UserDto, resetPasswordUrl: string, expirationTime: string): string => {
    return `
      <!DOCTYPE html>
      <html lang="en">
      <head>
        <meta charset="UTF-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <title>Réinitialisation de mot de passe</title>
      </head>
      <body style="font-family: Arial, sans-serif;">
  
        ${user.firstname && user.lastname ? `<p>Bonjour ${user.firstname} - ${user.lastname},</p>` : `<p>Bonjour,</p>`}
  
        <p>Nous avons reçu une demande de réinitialisation de mot de passe pour votre compte. Si vous n'avez pas demandé cette réinitialisation, veuillez ignorer ce message.</p>
  
        <p>Cliquez sur le lien ci-dessous pour réinitialiser votre mot de passe :</p>
  
        <p><a href="${resetPasswordUrl}">Réinitialiser le mot de passe</a></p>
  
        <p>Le lien expirera dans ${expirationTime}.</p>
  
        <p>Merci,<br>La Comté du dev</p>
  
      </body>
      </html>
    `;
  }
  