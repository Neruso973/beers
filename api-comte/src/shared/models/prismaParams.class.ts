export class PrismaQueryParams<T> {
    data?: T;
    where?: any;
    include?: any;
}