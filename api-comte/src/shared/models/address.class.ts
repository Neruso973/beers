export class Address {
    address: string;
    addressComplement: string;
    city: string;
    zipCode: string;
    country: string;
}