import { Module } from '@nestjs/common';
import { PrismaService } from './services/prisma.service';
import { ConfigService } from '@nestjs/config';
import { LogService } from 'src/routes/log/log.service';
import { MapperService } from './services/mapper.service';
import { MailService, QueryParamsService } from '.';
import { AccessTokenService } from './services/access-token.service';
import { JwtService } from '@nestjs/jwt';
import { AuthService } from 'src/routes/auth/auth.service';
import { UsersService } from 'src/routes/users/users.service';
import { DateHelperService } from './services/date-helper.service';
import { CronService } from './services/cron.service';

@Module({
  exports: [
    PrismaService,
    ConfigService,
    LogService,
    MapperService,
    QueryParamsService,
    AccessTokenService,
    JwtService,
    AuthService,
    UsersService,
    DateHelperService,
    MailService,
  ],
  providers: [
    PrismaService,
    ConfigService,
    LogService,
    MapperService,
    QueryParamsService,
    AccessTokenService,
    JwtService,
    AuthService,
    UsersService,
    DateHelperService,
    CronService,
    MailService,
  ],
})
export class SharedModule {}
