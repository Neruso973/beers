import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { AccessTokenService } from '../services/access-token.service';

@Injectable()
export class CurrentUserInterceptor implements NestInterceptor {
  constructor(private readonly _accessToken: AccessTokenService) {}

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers['authorization'];

    if (authHeader) {
      const token = authHeader.replace('Bearer ', '');

      const decodedToken = this._accessToken.getCurrentUser(token);

      request.currentUser = decodedToken;
    }
    return next.handle();
  }
}
