import { CallHandler, ExecutionContext, Injectable, NestInterceptor } from '@nestjs/common';
import { Observable } from 'rxjs';
import { PrismaQueryParams, QueryParamsService } from '../index';

@Injectable()
export class QueryParamsInterceptor implements NestInterceptor {
  private _queryParamService = new QueryParamsService();

  intercept(context: ExecutionContext, next: CallHandler): Observable<any> {
    //get request
    const request = context.switchToHttp().getRequest();
    if(request.method !== 'GET') {
      return next.handle();
    }
    // create prisma object from request
    let prismaParam: PrismaQueryParams<any> = {
      ...request?.query,
    };
    
    if(request.params && Object.keys(request.params).length > 0) {
      prismaParam.where = {
        ...request.params
      }
    }
    
    if(request.query) {
      prismaParam = this._queryParamService.formatQueryParams(prismaParam)

      if (request.body !== null && Object.keys(request.body).length > 0) {
        // set data property to body
        prismaParam.data = request.body;
      }

      
      //update body with full prisma object
      request.body = prismaParam;
      this._queryParamService.setPrismaQueryParams(prismaParam);
    }
    return next.handle();
  }
}
