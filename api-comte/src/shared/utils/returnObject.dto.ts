export class ReturnObject {
    code: number;
    message: string;

    constructor(returnObject: ReturnObject) {
        this.code = returnObject.code;
        this.message = returnObject.message;
    }
}