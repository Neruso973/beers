export const apiSuccess = {
    auth: {
        createDefaultAdmin: "create new admin",
        createDevelopementAccount: "create dev account",
        forgotPassword: "an email has been sent to reset your password",
        verifyEmail: "your will receive an email to verify your account",
    },
    save: {
        getUser: "Les données ont été sauvegardé",
        setUser: "Les données ont été enregistrées dans la table user"
    }
};