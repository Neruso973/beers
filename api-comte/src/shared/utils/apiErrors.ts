export const apiErrors = {
    common: {
        notAllowedInProduction: "Not allowed in production",
    },
    logs: {
        notFound: "No log found",
        badEnum: "bad enumeration",
        notFoundToUpdate: "No log found to be updated",
        notFoundToDelete: "No log found to be deleted",
        notUpdated: "Log not updated",
        notCreated: "Log not created",
    },
    users: {
        notFound: "No user found",
        notFoundToUpdate: "No user found to be updated",
        notFoundToDelete: "No user found to be deleted",
        notUpdated: "User not updated",
        notCreated: "User not created",
        notDeleted: "User not deleted",
        unauthorized: "you tried to update a user that is not yours",
        notActive: "User not active please verify your email or re send email validation",
    },
    auth: {
        userAlreadyExists: "User already exists",
        passwordNotMatch: "Passwords and confirm password does not match",
        invalidCredentials: "Invalid credentials",
        tokenExpired: "Token expired",
    },
    beers: {
        notFound: "No beer found",
        notFoundToUpdate: "No beer found to be updated",
        notFoundToDelete: "No beer found to be deleted",
        notUpdated: "Beer not updated",
        notCreated: "Beer not created",
        notDeleted: "Beer not deleted",
        unauthorized: "you tried to update a beer that is not yours",
        notActive: "Beer not active please verify your email or re send email validation",
    },
};