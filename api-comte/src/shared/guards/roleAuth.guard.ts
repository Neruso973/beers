import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { ConfigService } from '@nestjs/config';
import { JwtService } from '@nestjs/jwt';
import { AccessTokenService, ROLES_KEY } from '../';

@Injectable()
export class RoleAuthGuard implements CanActivate {
  constructor(
    private readonly reflector: Reflector,
    private readonly _config: ConfigService,
    private readonly _jwt: JwtService
  ) {}
  private _accessToken = new AccessTokenService(this._jwt, this._config);
  
  canActivate(context: ExecutionContext): boolean {
    const requiredRoles = this.reflector.getAllAndOverride<string[]>(
      ROLES_KEY,
      [context.getHandler(), context.getClass()],
    );

    if (!requiredRoles) {
      return true;
    }

    //get token from request
    const request = context.switchToHttp().getRequest();
    const authHeader = request.headers['authorization'];
    const token = authHeader?.replace('Bearer ', '');

    if (token) {
      const decodedToken = this._accessToken.getCurrentUser(token);
      if (requiredRoles.includes(decodedToken.role[0])) {
        return true;
      }
      return false;
    }
  }
}
