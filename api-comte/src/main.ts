import { NestFactory } from '@nestjs/core';
import { AppModule } from './app.module';
import { DocumentBuilder, SwaggerModule } from '@nestjs/swagger';

async function bootstrap() {
  const app = await NestFactory.create(AppModule, {
    snapshot: true,
  });

  //enable CORS
  app.enableCors({ origin: '*' });

  //swagger
  const config = new DocumentBuilder()
    .setTitle('la comté du dev')
    .setDescription('API description')
    .setVersion('1.0')
    .addTag('Beer')
    .addBearerAuth(
      { type: 'http', scheme: 'bearer', bearerFormat: 'JWT' },
      'jwt',
    )
    .build();

  const document = SwaggerModule.createDocument(app, config);
  SwaggerModule.setup('documentation', app, document);


  await app.listen(8080);
}
bootstrap();
