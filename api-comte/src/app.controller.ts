import { Controller, Get, UseGuards } from '@nestjs/common';
import { AppService } from './app.service';
import { AuthorizeRoles, RoleAuthGuard } from './shared';
import { RoleEnum } from '@prisma/client';
import { ApiBearerAuth, ApiTags } from '@nestjs/swagger';

@ApiTags('api')
@Controller('api')
export class AppController {
  constructor(private readonly appService: AppService) {}

  @ApiBearerAuth('jwt')
  @AuthorizeRoles(RoleEnum.ADMIN)
  @UseGuards(RoleAuthGuard)
  @Get()
  getHello(): string {
    return this.appService.getHello();
  }
}
