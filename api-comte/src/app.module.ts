import { Module } from '@nestjs/common';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { ConfigModule } from '@nestjs/config';
import { DevtoolsModule } from '@nestjs/devtools-integration';
import { LogModule } from './routes/log/log.module';
import { UsersModule } from './routes/users/users.module';
import { APP_INTERCEPTOR } from '@nestjs/core';
import { QueryParamsInterceptor } from './shared/interceptors/query-params.interceptor';
import { CurrentUserInterceptor } from './shared/interceptors/current-user.interceptor';
import { SharedModule } from './shared/shared.module';
import { AccessTokenService, BootstrapService } from './shared';
import { ScheduleModule } from '@nestjs/schedule';
import { BeersModule } from './routes/beers/beers.module';
import { HopModule } from './routes/hop/hop.module';
import { MaltModule } from './routes/malt/malt.module';

@Module({
  imports: [
    LogModule,
    ScheduleModule.forRoot(),
    DevtoolsModule.register({
      http: process.env.NODE_ENV !== 'production',
    }),
    ConfigModule.forRoot(),
    UsersModule,
    SharedModule,
    BeersModule,
    HopModule,
    MaltModule,
  ],
  controllers: [AppController],
  providers: [
    AppService,
    AccessTokenService,
    BootstrapService,
    {
      provide: APP_INTERCEPTOR,
      useClass: QueryParamsInterceptor,
    },
    {
      provide: APP_INTERCEPTOR,
      useClass: CurrentUserInterceptor,
    },
  ],
})
export class AppModule {}
